from django.contrib import admin

# Register your models here.
from .models import Residente,Informe,Familiar,ParteInforme

admin.site.register(Residente)
admin.site.register(Informe)
admin.site.register(Familiar)
admin.site.register(ParteInforme)